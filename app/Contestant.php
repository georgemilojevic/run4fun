<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contestant extends Model
{
    protected $fillable = [
        'bib_number', 'name', 'gender', 'dob', 'age', 'phone', 'email', 'country'
    ];

    /**
     * Get the contestant that belongs to the races.
     */
    public function races()
    {
        return $this->belongsToMany('App\Race');
    }

    /**
     * Get the contestant that belongs to the races.
     */
    public function distances()
    {
        return $this->belongsToMany('App\Distance');
    }
}
