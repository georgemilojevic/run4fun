<?php

namespace App\Http\Controllers;

use App\User;
use App\Race;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class UserController extends Controller
{
    public function showRaces($id)
    {
        $races = User::with('races')->find($id);

            foreach ($races->races as $race) {
                $all_races[] = [
                    'name'   => $race->name,
                    'place'  => $race->place,
                    'date'   => $race->date,
                    'time'   => $race->time,
                    'status' => $race->status,
                ];
            }
        return $all_races;
    }

    public function storeRacesUsers(Request $request, $user_id)
    {
        $race = Race::create($request->all());

        User::find($user_id)->races()->save($race);

        return response()->json([$race, $user_id], 201);
    }
}