<?php

namespace App\Http\Controllers;

use App\Contestant;
use App\Distance;
use App\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class ContestantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Contestant::all();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contestant = Contestant::create($request->all());

        return response()->json($contestant, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contestant  $contestant
     * @return \Illuminate\Http\Response
     */
    public function show(Contestant $contestant)
    {
        return $contestant;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contestant  $contestant
     * @return \Illuminate\Http\Response
     */
    public function edit(Contestant $contestant)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contestant  $contestant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contestant $contestant)
    {
        $contestant->update($request->all());

        return response()->json($contestant, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contestant  $contestant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contestant $contestant)
    {
        $contestant->delete();

        return response()->json(204);
    }

    public function showDistances($id) {

        $distances = Contestant::with('distances')->find($id);
        return $distances;

    }

    public function storeContestantsDistances(Request $request, $contestant_id)
    {
        $distance = Distance::create($request->all());

        Contestant::find($contestant_id)->distances()->save($distance);

        return response()->json([$distance, $contestant_id], 201);
    }
}
