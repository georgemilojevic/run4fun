<?php

namespace App\Http\Controllers;

use App\Contestant;
use App\Race;
use App\Distance;
use Illuminate\Http\Request;

class RaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Race::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $race = Race::create($request->all());

        return response()->json($race, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Race  $race
     * @return \Illuminate\Http\Response
     */
    public function show(Race $race)
    {
        return $race;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Race  $race
     * @return \Illuminate\Http\Response
     */
    public function edit(Race $race)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Race  $race
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Race $race)
    {
        $race->update($request->all());

        return response()->json($race, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Race  $race
     * @return \Illuminate\Http\Response
     */
    public function destroy(Race $race)
    {
        $race->delete();

        return response()->json(204);
    }

    public function showContestant($id) {

        $contestant = Contestant::with('races')->find($id);

        foreach ($contestant->races as $race) {
            $all[] = [
                'name'   => $race->name,
                'place'  => $race->place,
                'date'   => $race->date,
                'time'   => $race->time,
                'status' => $race->status
            ];
        }
        return $all;
    }

    public function showRacesContestants($id) {

        $contestants = Race::with('contestants')->find($id);

            foreach ($contestants->contestants as $contestant) {
                $all_contestant[] = [
                    'name'    => $contestant->name,
                    'gender'  => $contestant->gender,
                    'dob'     => $contestant->dob,
                    'age'     => $contestant->age,
                    'phone'   => $contestant->phone,
                    'email'   => $contestant->email,
                    'country' => $contestant->country
                ];
            }
        return $all_contestant;
    }

    /**
     * @param $id
     * @return array
     *
     * Show all Distances that belong to a race
     */
    public function showRacesDistances($id) {

        $distances = Race::with('distances')->find($id);

        foreach ($distances->distances as $distance) {
            $all_distances[] = [
                'name'     => $distance->name,
                'distance' => $distance->distance,
                'unit'     => $distance->unit
            ];
        }
        return $all_distances;
    }

    /**
     * @param Request $request
     * @param $race_id
     * @return \Illuminate\Http\JsonResponse
     *
     * Saves a Contestant to existing race
     */
    public function storeRacesContestants(Request $request, $race_id)
    {
        $contestant = Contestant::create($request->all());

        Race::find($race_id)->contestants()->save($contestant);

        return response()->json([$contestant, $race_id], 201);
    }

    /**
     * @param Request $request
     * @param $race_id
     * @return \Illuminate\Http\JsonResponse
     *
     * Saves a Distance to existing race
     */
    public function storeRacesDistances(Request $request, $race_id)
    {
        $distances = Distance::create($request->all());

        Race::find($race_id)->distances()->save($distances);

        return response()->json([$distances, $race_id], 201);
    }

}
