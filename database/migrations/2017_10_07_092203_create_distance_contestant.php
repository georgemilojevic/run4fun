<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistanceContestant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contestant_distance', function (Blueprint $table) {
            $table->integer('contestant_id')->unsigned();
            $table->foreign('contestant_id')->references('id')->on('contestants')->onDelete('cascade');
            $table->integer('distance_id')->unsigned();
            $table->foreign('distance_id')->references('id')->on('distances')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('contestant_distance');
        Schema::enableForeignKeyConstraints();
    }
}
