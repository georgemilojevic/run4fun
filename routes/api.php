<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Auth::routes();

Route::get('/user/{user}', function (App\user $user) {
    return $user->email;
});

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

# CONTESTANT ROUTES
Route::middleware('auth:api')->group(function () {
    Route::resource('contestants', 'ContestantController');
});

# DISTANCES ROUTES
Route::middleware('auth:api')->group(function () {
    Route::resource('distances', 'DistanceController');
});

# RACES ROUTES
Route::middleware('auth:api')->group(function () {
    Route::resource('races', 'RaceController');
});


# GROUPED AUTH:API GET ROUTES
Route::middleware('auth:api')->group(function () {

# SHOW ONE DISTANCE THAT BELONGS TO MANY CONTESTANTS
    Route::get('/distance/{id}/distancecontestant', 'DistanceController@showDistanceContestant')
        ->name('DistanceContestant');

# SHOW ALL RACES THAT BELONGS TO A SINGLE CONTESTANT
    Route::get('/race/contestant/{id}', 'RaceController@showContestant')
        ->name('showContestant');

# SHOW ALL RACES THAT BELONG TO A USER
    Route::get('/user/{id}/races', 'UserController@showRaces')
        ->name('showRaces');

# SHOW THE CONTESTANT AND ALL OF THE RECORDED DISTANCES
    Route::get('contestant/{id}/distances', 'ContestantController@showDistances')
        ->name('showDistances');

# SHOW ALL CONTESTANTS THAT BELONG TO A RACE
    Route::get('race/{id}/racescontestants', 'RaceController@showRacesContestants')
        ->name('showRacesContestants');

    # SHOW ALL DISTANCES THAT BELONG TO A RACE
    Route::get('race/{id}/racesdistances', 'RaceController@showRacesDistances')
        ->name('showRacesContestants');
});


# SAVE RACES TO THE USER
Route::post('/user/{user_id}/storeRacesUsers',
    'UserController@storeRacesUsers')
    ->middleware('auth:api');

# SAVE CONTESTANT TO THE RACE
Route::post('/race/{race_id}/storeRacesContestants',
    'RaceController@storeRacesContestants')
    ->middleware('auth:api');

# SAVE DISTANCE TO THE RACE
Route::post('/race/{race_id}/storeRacesDistances',
    'RaceController@storeRacesDistances')
    ->middleware('auth:api');

# SAVE DISTANCE TO THE CONTESTANT
Route::post('/contestant/{contestant_id}/storeContestantsDistances',
    'ContestantController@storeContestantsDistances')
    ->middleware('auth:api');

//Route::get('/oauth/token', function(Request $request) {
//    $tokenRequest = request('/oauth/token', get());
//    dd($tokenRequest);
//    return 'api/user';
//});

